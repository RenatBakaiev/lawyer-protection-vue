Vue.component('footer_lp', {
    template: `
       <div class="footer_lp">
        <div class="footer_in">
            <div class="footer_menu">
                <p class="footer_menu_item">главная</p>
                <img src="pic/footer_romb.svg" class="footer_romb">
                <p class="footer_menu_item">услуги</p>
                <img src="pic/footer_romb.svg" class="footer_romb">
                <p class="footer_menu_item">о адвокате</p>
                <img src="pic/footer_romb.svg" class="footer_romb">
                <p class="footer_menu_item">преимущества</p>
                <img src="pic/footer_romb.svg" class="footer_romb">
                <p class="footer_menu_item">сертификаты</p>
                <img src="pic/footer_romb.svg" class="footer_romb">
                <p class="footer_menu_item">контакты</p>
            </div>
            <div class="footer_signs">
                <img src="pic/footer_sign_1.svg" class="footer_sign" onclick="openFacebook()">
                <img src="pic/footer_sign_2.svg" class="footer_sign" onclick="openInstagram()">
                <img src="pic/footer_sign_3.svg" class="footer_sign" onclick="openTwitter()">
                <img src="pic/footer_sign_4.svg" class="footer_sign" onclick="openGoogle()">
            </div>
        </div>
    </div>
    `,
});